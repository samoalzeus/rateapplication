﻿using RatingApplication.Domain.Models;
using RatingApplication.Domain.Service;
using RatingApplication.Web.Service;
using System.Web.Mvc;

namespace RatingApplication.Web.Areas.Api.Controllers
{
    public class SkillApiController : Controller
    {
        private ISkillService skillService;
        public SkillApiController()
        {
            //OBS! Skippar Dependecy injections for nu.. 
            skillService = new SkillService();
        }


        [HttpGet]
        public ActionResult GetSkills()
        {
            var skills = skillService.GetSkills();
            return Json(skills, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddSkill(Skill skill)
        {
            return Json(skillService.AddSkill(skill));
        }

        [HttpPost]
        public ActionResult RemoveSkill(int skillId)
        {
            return Json(skillService.RemoveSkill(skillId));
        }

        [HttpPost]
        public ActionResult UpdateSkill(Skill skill)
        {
            return Json(skillService.Update(skill));
        }
    }
}