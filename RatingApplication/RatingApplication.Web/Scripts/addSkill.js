﻿var Skill = function (obj) {
    var self = this;
    self.Id = ko.observable(obj.Id);
    self.Name = ko.observable(obj.Name);
    self.Rate = ko.observable(obj.Rate);
    self.newRate = ko.observable(obj.Rate);
}

var mainModel = function () {
    var self = this;
    self.skills = ko.observableArray();
    self.skillName = ko.observable();
    self.skillRate = ko.observable();

    self.active = ko.observable(false);
    self.setRate = function (rate) {
        self.skillRate(rate);
        self.active(rate);
    }

    self.addSkill = function () {
        console.log(self.skillName());
        if (self.skillName() === undefined || self.skillName() === "") {
            alert("You need to give a skill name.. (cool alert popup/message)");
            return
        }
        var skill = new Skill({ Name: self.skillName(), Rate: self.skillRate() });
        $.post("Api/SkillApi/AddSkill", { skill: skill }, function (response) {
            self.skills.push(new Skill(response));
        });
        self.skillName("");
        self.skillRate(0);
        self.active(0);

    }

    var currentSkillRate;
    self.changeRate = function (newRate, skill) {
        skill.newRate(newRate);
    }

    self.updateSkill = function (d, skillToUpdate) {
        skillToUpdate.Rate(skillToUpdate.newRate());
        $.post("Api/SkillApi/UpdateSkill", { skill: skillToUpdate }, function (response) {
            var updatedSkill = self.skills().filter(function (skill) {
                return skill.Id == skillToUpdate.Id();
            });

            updatedSkill.Rate(response.Rate);
            updatedSkill.newRate(response.Rate);
        });
    }

    self.removeSkill = function (skillId) {
        $.post("Api/SkillApi/RemoveSkill", { skillId: skillId }, function (response) {
            if (response) {
                self.skills(self.skills().filter(function (skill) {
                    return skill.Id !== skillId;
                }));
            }
        });
    }

    $.get("Api/SkillApi/GetSkills", function (response) {
        self.skills($.map(response, function (skill) {
            return new Skill(skill);
        }));
    });
}
ko.applyBindings(mainModel);

