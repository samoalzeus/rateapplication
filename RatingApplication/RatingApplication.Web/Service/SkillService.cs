﻿using RatingApplication.Domain.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RatingApplication.Domain.Models;
using RatingApplication.Domain.Repository;

namespace RatingApplication.Web.Service
{
    public class SkillService : ISkillService
    {
        ISkillRepository repository;
        public SkillService()
        {
            //OBS! Skippar Dependecy injections for nu.. 
            repository = new SkillRepository();
        }

        public Skill AddSkill(Skill skill)
        {
            return repository.AddSkill(skill);
        }

        public Skill Get(int id)
        {
            return repository.Get(id);
        }

        public List<Skill> GetSkills()
        {
            return repository.GetSkills();
        }

        public bool RemoveSkill(int id)
        {
           return repository.RemoveSkill(id);
        }

        public Skill Update(Skill skill)
        {
            return repository.Update(skill);
        }
    }
}