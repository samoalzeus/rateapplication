﻿using RatingApplication.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RatingApplication.Domain.Service
{
    public interface ISkillService
    {
        Skill AddSkill(Skill skill);
        bool RemoveSkill(int id);
        List<Skill> GetSkills();
        Skill Get(int id);
        Skill Update(Skill skill);
    }
}
