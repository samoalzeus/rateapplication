﻿using RatingApplication.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RatingApplication.Domain.Repository
{
    public interface ISkillRepository
    {
        List<Skill> GetSkills();
        Skill AddSkill(Skill skill);
        bool RemoveSkill(int id);
        Skill Get(int id);
        Skill Update(Skill skill);
    }
}
