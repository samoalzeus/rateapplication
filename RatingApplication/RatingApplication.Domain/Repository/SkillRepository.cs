﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RatingApplication.Domain.Models;
using RatingApplication.Domain.Context;

namespace RatingApplication.Domain.Repository
{
    public class SkillRepository : ISkillRepository
    {
        RatingApplicationDbContext db;
        public SkillRepository()
        {
            db = new RatingApplicationDbContext();
        }

        public Skill AddSkill(Skill skill)
        {
            db.Skills.Add(skill);
            db.SaveChanges();
            return skill;
        }

        public Skill Get(int id)
        {
            return db.Skills.FirstOrDefault(s => s.Id == id);
        }

        public List<Skill> GetSkills()
        {
            return db.Skills.ToList();
        }

        public bool RemoveSkill(int id)
        {
            var skill = db.Skills.FirstOrDefault(s => s.Id == id);
            db.Skills.Remove(skill);
            db.SaveChanges();
            return !db.Skills.Any(s => s.Id == id);
        }

        public Skill Update(Skill skill)
        {
            var currentSkill = db.Skills.FirstOrDefault(s => s.Id == skill.Id);
            if (currentSkill != null)
            {
                currentSkill.Name = skill.Name;
                currentSkill.Rate = skill.Rate;
                db.SaveChanges();
            }
            return currentSkill != null ? currentSkill : skill;
        }
    }
}
