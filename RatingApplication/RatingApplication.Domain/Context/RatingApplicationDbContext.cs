﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using RatingApplication.Domain.Models;

namespace RatingApplication.Domain.Context
{
    public class RatingApplicationDbContext : DbContext
    {
        public RatingApplicationDbContext() : base("SkillsConnection")
        {
            Database.SetInitializer(new InitilizeDb());
        }

        public DbSet<Skill> Skills { get; set; }
    }

    public class InitilizeDb : DropCreateDatabaseAlways<RatingApplicationDbContext>
    {
        protected override void Seed(RatingApplicationDbContext context)
        {
            var skillsList = new List<Skill>
            {
                new Skill { Id = 1, Name = "Javascript", Rate = 4 },
                new Skill { Id = 1, Name = "C#", Rate = 5 },
                new Skill { Id = 1, Name = "Photoshop", Rate = 3 },
            };

            context.Skills.AddRange(skillsList);

            base.Seed(context);
        }
    }
}
